# deploy_all_website_script

This bash script will get all the relevant git repository and configuration to automatically configure my RPI with all the website I use.

## Ideas list :

- fetch all websites repository (personalwebsite - festival) - OK
- + on other location tools - OK
- put the apache2 sites configuration on a repo + copy or symbolics links to /etc/apache2/site-available - Copy OK - ln WIP
- a2en-site - OK
- install required packages - WIP
- create configuration file - Still relevant ??
- pgp the configuration file - Still relevant ??
- create symbolic links to /var/www/html - WIP
- wget webmin and owncloud - TODO
- webmin + owncloud install - TODO
- certbot - TODO
- check git configuration + ssh key are present - Still relevant ??
- install and configure db (mariadb) - TODO
- add print log to file option
- add test option - WIP
