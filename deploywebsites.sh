#!/bin/bash

DATE=`date`
GIT_DIR="/home/kerboitk/workspace/git"
TOOLS_DIR="$GIT_DIR/tools"
WEBSITES_DIR="$GIT_DIR/websites"
PACKAGE_MANAGER="apt"
MANIFEST_WEBSITES_PATH="$WEBSITES_DIR/.repo/manifests/websites.xml"
MANIFEST_TOOLS_PATH="$TOOLS_DIR/.repo/manifests/tools.xml"
SYM_LINK=false
WWW_DIR="/var/www/html"
TEST=false

printLog()
{
  echo -e "[$DATE] > $1"
}

goToDir()
{
  printLog "Go to directory : $1"
  pushd $1 > /dev/null
  printLog ""
}

returnLastDir()
{
  printLog "return to the previous directory : "
  popd
  printLog ""
}

confirm()
{
  # call with a prompt string or use a default
  read -r -p "${1:-Are you sure? [y/N]} " response

  case "$response" in

	  [yY][eE][sS]|[yY])
      true
      ;;

	  [nN][oO]|[nN])
		  false
		  ;;

    *)
      printLog "Invalid choise.."
	    false
      ;;
  esac
}

isRepoInstalled()
{
  returnCode=true

  printLog "is repo installed ?"

	which repo > /dev/null

	if [ ! $? -eq 0 ]
	then
		printLog "Repo is not installed."

		if confirm "Do you want to install it ? [y/N]"
		then
			sudo $PACKAGE_MANAGER install repo
		else
			printLog "Quit.."
			returnCode=false
		fi

	else
		printLog "OK"
	fi

	printLog ""

	$returnCode
}

isCertbotInstalled()
{
	returnCode=true

  printLog "is certbot installed ?"

	which certbot > /dev/null

	if [ ! $? -eq 0 ]
	then
		printLog "certbot is not installed."

		if confirm "Do you want to install it ? [y/N]"
		then
			sudo $PACKAGE_MANAGER install certbot
		else
			printLog "Quit.."
			returnCode=false
		fi

	else
		printLog "OK"
	fi

	printLog ""

  $returnCode
}

isApacheInstalled()
{
  returnCode=true

	printLog "is apache2 installed ?"

	systemctl status apache2 > /dev/null

	if [ ! $? -eq 0 ]
	then
		printLog "apache2 is not installed."

		if confirm "Do you want to install it ? [y/N]"
		then
			sudo $PACKAGE_MANAGER install apache2
		else
			printLog "Quit.."
	    returnCode=false
		fi

	else
		printLog "OK"
	fi

	printLog ""

	$returnCode
}

doesGitDirExist()
{
	returnCode=true

  printLog "does Git directory exist ?"

	if [ -d "$GIT_DIR" ]
	then
		printLog "OK"
	else
		printLog "Git directory does not exist"

		if confirm "Do you want to create the $GIT_DIR directory ? [y/N]"
		then
			mkdir -p "$GIT_DIR"
		else
			printLog "Quit.."
			returnCode=false
		fi
	fi

	printLog ""

  $returnCode
}


doesWebsitesDirExist()
{
	returnCode=true

  printLog "does Websites directory exist ?"

	if [ -d "$WEBSITES_DIR" ]
	then
		printLog "OK"
	else
		printLog "Website directory does not exist"

		if confirm "Do you want to create the $WEBSITES_DIR directory ? [y/N]"
		then
			mkdir -p "$WEBSITES_DIR"
		else
			printLog "Quit.."
			returnCode=false
		fi
	fi

	printLog ""

  $returnCode
}


doesToolsDirExist()
{
	returnCode=true

  printLog "does Tools directory exist ?"

	if [ -d "$TOOLS_DIR" ]
	then
		printLog "OK"
	else
		printLog "Tools directory does not exist"

		if confirm "Do you want to create the $TOOLS_DIR directory ? [y/N]"
		then
			mkdir -p "$TOOLS_DIR"
		else
			printLog "Quit.."
			returnCode=false
		fi
	fi

	printLog ""

  $returnCode
}

getWebsitesGitRepos()
{
  returnCode=true

  printLog "get all websites git repositories.."
  printLog ""
  goToDir "$WEBSITES_DIR"

  printLog "repo init"
  repo init -u git@gitlab.com:Tchev146/project_manifest.git -m websites.xml

  printLog ""

  if [ ! $? -eq 0 ]
  then
    printLog "An error occured while repo init.."
    printLog "quitting"
    returnCode=false
  else
    printLog "repo sync"
    repo sync
  fi

  returnLastDir
  printLog ""

  $returnCode
}

getToolsGitRepos()
{
  returnCode=true

  printLog "get all tools git repositories.."
  printLog ""
  goToDir "$TOOLS_DIR"

  printLog "repo init"
  repo init -u git@gitlab.com:Tchev146/project_manifest.git -m tools.xml

  printLog ""

  if [ ! $? -eq 0 ]
  then
    printLog "An error occured while repo init.."
    printLog "quitting"
    returnCode=false
  else
    printLog "repo sync"
    repo sync
  fi

  returnLastDir
  printLog ""

  $returnCode
}

## take the path of websites or tools in parameter
checkGitBranch()
{
  printLog "check all the git branch"
  goToDir "$1"

  REPOS=`ls -1`

  for repo in $REPOS
  do
    PATHREPO="$1/$repo"

    pushd $PATHREPO > /dev/null

    # Method bellow in newer version of git.. unavailable on rpi
    # BRANCH=`git branch --show-current`

    BRANCH=`git rev-parse --abbrev-ref HEAD`
    popd > /dev/null

    printLog "Git repo = $repo, path = $PATHREPO, branch = $BRANCH"
	  printLog ""
  done

  returnLastDir
  printLog ""
  true
}

copyAndApplyApache2Config()
{
  printLog "copy and apply apache2 config files.."

  rsync apache_conf_sites/* /etc/apache2/sites_available

  goToDir /etc/apache2/site_available

  ls -1 | xargs a2ensite

  returnLastDir
  printLog ""
  true
}

## WIP
lnAndApplyApache2Config()
{
  printLog "create symlink and apply apache2 config files.."


  goToDir /etc/apache2/site_available

  ls -1 | xargs a2ensite

  returnLastDir
  printLog ""
  true
}


## take the path of websites or tools in parameter
checkoutBranches()
{
	printLog "parsing manifest file"
	printLog ""

  if [[ "$1" == "$WEBSITES_DIR" ]]
  then
    MANIFEST_PATH="$MANIFEST_WEBSITES_PATH"
  fi

  if [[ "$1" == "$TOOLS_DIR" ]]
  then
    MANIFEST_PATH="$MANIFEST_TOOLS_PATH"
  fi

  printLog "Manifest Path : $MANIFEST_PATH"
  printLog ""

	names=( $(cat $MANIFEST_PATH | grep "project" | awk -F'"' '{print $6}') )
	revision=( $(cat $MANIFEST_PATH | grep "project" | awk -F'"' '{print $8}') )

	goToDir "$1"

	printLog "checkout all repo on correct branch"
	printLog ""

	for (( i=0; i<${#names[@]}; i++ ))
	do
		printLog "repo start ${revision[i]} ${names[i]}"
		repo start ${revision[i]} ${names[i]}
		printLog ""
	done

	returnLastDir
	printLog ""
  true
}

lnWebsitesGitDir()
{
  printLog "Create symbolic link of all project in /var/www/html"
  printLog ""

  goToDir "$WWW_DIR"

  names=( $(ls $WEBSITES_DIR) )

  for(( i=0; i<${#names[@]}; i++))
  do
    printLog "ln -s $WEBSITES_DIR/${names[i]} ${names[i]}"
    #ln -s $WEBSITES_DIR/${names[i]} ${names[i]}
  done

  returnLastDir

  printLog ""
  true
}

help()
{
	printLog "$0 : Usage : "
	printLog "\t example : this is an example."
}

################################################
#### Main
################################################

while getopts "hst" OPTION
do
	case $OPTION in

		h)
			help
			;;

    s)
      SYM_LINK=true
      ;;

    t)
      TEST=true
      ;;

		*)
			help
			exit 1
			;;

	esac
done

if ! isRepoInstalled
then
	exit 2
fi

if ! isCertbotInstalled
then
	exit 3
fi

if ! isApacheInstalled
then
	exit 4
fi

if ! doesGitDirExist
then
	exit 5
fi

if ! doesWebsitesDirExist
then
	exit 6
fi

if ! doesToolsDirExist
then
	exit 7
fi

if ! getWebsitesGitRepos
then
	exit 8
fi

## WIP
#if $SYM_LINK -eq true
#then
# if ! lnAndApplyApache2Config
# then
#   exit 6
# fi
#else
# if ! copyAndApplyApache2Config
# then
#   exit 6
# fi
#fi

if ! checkoutBranches "$WEBSITES_DIR"
then
	exit 9
fi

if ! checkGitBranch "$WEBSITES_DIR"
then
	exit 10
fi

if ! getToolsGitRepos
then
  exit 11
fi

if ! checkoutBranches "$TOOLS_DIR"
then
	exit 12
fi

if ! checkGitBranch "$TOOLS_DIR"
then
	exit 13
fi

#WIP
if ! lnWebsitesGitDir
then
  exit 14
fi

exit 0
